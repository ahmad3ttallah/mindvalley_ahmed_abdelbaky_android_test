package com.abdelbaky.resourcesloader.lib;

abstract class Resource implements CallBack {
   private String url;
   protected Resource nextResource;
   private CallBack callBack;

   protected Resource(){
      callBack = this;
   }

   protected void setNextResource(Resource nextResource){
      this.nextResource = nextResource;
      this.nextResource.setCallBack(callBack);
   }

   protected void loadURL(){
      this.loadData(new CallBack() {
         @Override
         public void done(Object data) {
            if(data != null){
               callBack.done(data);
            }else if(nextResource != null){
               nextResource.setUrl(getUrl());
               nextResource.loadURL();
            }
         }
      }, url);
   };

   public void setUrl(String url) {
      this.url = url;
   }

   public String getUrl() {
      return url;
   }

   protected void setCallBack(CallBack callBack) {
      this.callBack = callBack;
   }

   abstract protected void loadData(CallBack resourceCallBack, String url);

   protected void mapData(String url, Object obj, long size){};
}