package com.abdelbaky.resourcesloader.lib;

public interface CallBack{
    void done(Object obj);
}