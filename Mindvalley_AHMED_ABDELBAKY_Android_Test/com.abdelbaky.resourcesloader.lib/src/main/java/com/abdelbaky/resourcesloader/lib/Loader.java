package com.abdelbaky.resourcesloader.lib;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;

/**
 * Created by ahmedabd-elbaky on 19/01/2017.
 */

public class Loader extends Cash {
    public static Map<String, List<Loader>> loadersPerUrl = new HashMap<>();

    private Activity activity;
    private Web web;
    private JSONCallback jsonCallback;
    private ImageView imageview;

    public static Loader with(Activity activity){
        return new Loader(activity);
    }

    private Loader(Activity activity){
        super();
        this.activity = activity;
        this.web = new Web() {
            @Override
            public void done(Object obj) {
                Loader.this.done(obj);
            }
        };
        setNextResource(web);
    }

    public Loader load(String url){
        setUrl(url);
        if(!cash.containsKey(url)){
            if(loadersPerUrl.get(url) == null){
                loadersPerUrl.put(url, new ArrayList<Loader>());
            }else{
                loadersPerUrl.get(url).add(this);
            }
        }
        return this;
    }

    public void done(Object obj) {
        if(obj == null)
            return;

        if(obj instanceof ResponseBody) {
            Log.d("ResponseBody TYPE", ((ResponseBody) obj).contentType().type());

            switch (((ResponseBody) obj).contentType().type()) {
                case "message":
                    handleText((ResponseBody) obj);
                    break;
                case "application":
                    handleText((ResponseBody) obj);
                    break;
                case "text":
                    handleText((ResponseBody) obj);
                    break;
                case "image":
                    InputStream inputStream = ((ResponseBody) obj).byteStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    if(bitmap != null){
                        setImageViewBitmap(bitmap);
                        mapData(getUrl(), bitmap, bitmap.getByteCount() / 1024); // kilobytes
                    }
                    break;
                case "audio":
                    break;
                case "video":
                    break;
            }
            notifyAllLoaders();
        }
        if(obj instanceof Bitmap) {
            setImageViewBitmap((Bitmap) obj);
            Log.d("IMAGE FROM CASH => ", "+");
        }else if(obj instanceof String){
            setJSONString((String)obj);
            Log.d("JSON FROM CASH", (String)obj);
        }
    }

    private void handleText(ResponseBody responseBody){
        try {
            String jsonString = responseBody.string();
            setJSONString(jsonString);
            mapData(getUrl(), jsonString, jsonString.getBytes().length / 1024);
        } catch (IOException e) {
            e.printStackTrace();
            setJSONString(null);
        }
    }

    private void setImageViewBitmap(final Bitmap bitmap){
        if(imageview != null && bitmap != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imageview.setImageBitmap(bitmap);
                }
            });
        }
    }

    private void setJSONString(final String jsonString){
        if(jsonCallback != null && jsonString != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    jsonCallback.json(jsonString);
                }
            });
        }
    }

    public Loader into(ImageView imageview){
        this.imageview = imageview;
        loadURL();
        return this;
    }

    public void into(JSONCallback jsonCallback){
        this.jsonCallback = jsonCallback;
        loadURL();
    }

    /**
     * This is a function that can perform cancel loading operation if the image view clicked
     * */
    public void enableCancelLoadingForImageView(boolean enable){
        if(imageview == null)
            return;
        if(enable)
            imageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Loader.this.cancel();
                }
            });
        else
            imageview.setOnClickListener(null);
    }

    public void cancel(){
        this.web.cancel();
    }

    public void notifyAllLoaders(){
        if(loadersPerUrl.get(getUrl()) != null)
            for(Loader loader : loadersPerUrl.get(getUrl())){
                loader.loadURL();
            }
        loadersPerUrl.remove(getUrl());
    }

    public static interface JSONCallback {
        public void json(String json);
    }
}
