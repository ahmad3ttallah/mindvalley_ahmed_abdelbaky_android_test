package com.abdelbaky.resourcesloader.lib;

/**
 * Created by ahmedabd-elbaky on 20/01/2017.
 */

public interface CashConfigurations {
    CashConfigurations removeCashedResource(String url);
    CashConfigurations setLifeTime(int lifeTime);
    CashConfigurations setSizeInBytesLimit(int sizeInBytes);
}
