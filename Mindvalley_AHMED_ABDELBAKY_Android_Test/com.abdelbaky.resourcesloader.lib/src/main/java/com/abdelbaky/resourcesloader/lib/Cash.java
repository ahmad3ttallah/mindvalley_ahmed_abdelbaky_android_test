package com.abdelbaky.resourcesloader.lib;

import java.util.HashMap;

/**
 * Created by ahmedabd-elbaky on 19/01/2017.
 */

abstract class Cash extends Resource implements CashConfigurations{
    protected static HashMap<String, Object> cash = new HashMap<>();
    private static HashMap<String, Long> lifeTimes = new HashMap<>();
    private static long lifeTime = Long.MAX_VALUE;
    private static int sizeInBytes = Integer.MAX_VALUE;

    @Override
    protected void loadData(CallBack callBack, String url) {
        if(lifeTimes.containsKey(url) && (System.currentTimeMillis() - lifeTimes.get(url)) <= lifeTime){
            callBack.done(cash.get(url));
        }else{
            cash.remove(url);
            callBack.done(null);
        }
    }

    @Override
    protected void mapData(String url, Object obj, long size) {
        if(!cash.containsKey(url)){
            if(size > 0 && size <= sizeInBytes){
                cash.put(url, obj);
                lifeTimes.put(url, System.currentTimeMillis());
            }
        }
    }

    @Override
    public CashConfigurations removeCashedResource(String url) {
        cash.remove(url);
        return this;
    }

    @Override
    public CashConfigurations setLifeTime(int lifeTime) {
        if(lifeTime > 0) // Validate lifetime which should be positive
            Cash.lifeTime = lifeTime;
        return this;
    }

    @Override
    public CashConfigurations setSizeInBytesLimit(int sizeInBytes) {
        if(sizeInBytes > 0) // Validate lifetime which should be positive
            Cash.sizeInBytes = sizeInBytes;
        return this;
    }
}
