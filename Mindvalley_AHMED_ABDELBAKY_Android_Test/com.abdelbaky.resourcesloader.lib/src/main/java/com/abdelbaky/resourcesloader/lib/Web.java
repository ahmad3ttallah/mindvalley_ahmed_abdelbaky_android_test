package com.abdelbaky.resourcesloader.lib;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ahmedabd-elbaky on 19/01/2017.
 */

abstract class Web extends Resource {
    public static List<String> processingUrls = new ArrayList<>();
    private OkHttpClient client;
    private Request request;
    private Call call;

    @Override
    protected void loadData(final CallBack callBack, final String url) {
        if(processingUrls.indexOf(url) != -1)
            return;
        Log.d("URL => ", url);
        processingUrls.add(url);
        client = new OkHttpClient();
        request = new Request.Builder().url(url).build();
        call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                processingUrls.remove(url);
                callBack.done(null);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                processingUrls.remove(url);
                callBack.done(response.body());
            }
        });
    }

    public void cancel(){
        if(call != null)
            call.cancel();
    }
}
