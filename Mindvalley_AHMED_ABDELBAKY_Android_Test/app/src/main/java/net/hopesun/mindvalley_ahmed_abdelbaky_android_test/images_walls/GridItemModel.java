package net.hopesun.mindvalley_ahmed_abdelbaky_android_test.images_walls;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by ahmedabd-elbaky on 22/01/2017.
 */

public class GridItemModel {

    public static GridItemModel parse(String jsonData) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        return objectMapper.readValue(jsonData, GridItemModel.class);
    }

    public static List<GridItemModel> parseList(String jsonData) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        return objectMapper.readValue(jsonData, TypeFactory.defaultInstance().constructCollectionType(List.class, GridItemModel.class));
    }

    public String id;
    public String created_at;
    public int width;
    public int height;
    public String color;
    public int likes;
    public boolean liked_by_user;
    public User user;
    public List<String> current_user_collections;
    public Urls urls;
    public List<Category> categories;

    public static class Urls {
        public String raw;
        public String full;
        public String regular;
        public String small;
        public String thumb;
    }

    public static class Links {
        public String self;
        public String html;
        public String photos;
        public String likes;
        public String download;
    }

    public static class Category {
        public int id;
        public String title;
        public int photo_count;
        public Links links;
    }

    public static class ProfileImage {
        public String small;
        public String medium;
        public String large;
    }

    public static class User {
        public String id;
        public String username;
        public String name;
        public ProfileImage profile_image;
        public Links links;
    }
}