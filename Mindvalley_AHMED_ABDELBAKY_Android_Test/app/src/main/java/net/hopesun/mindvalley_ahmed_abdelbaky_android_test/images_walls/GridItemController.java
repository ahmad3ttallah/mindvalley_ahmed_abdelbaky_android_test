package net.hopesun.mindvalley_ahmed_abdelbaky_android_test.images_walls;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abdelbaky.resourcesloader.lib.Loader;

import net.hopesun.mindvalley_ahmed_abdelbaky_android_test.R;

/**
 * Created by ahmedabd-elbaky on 23/01/2017.
 */

public class GridItemController {
    private View view;
    public GridItemController(Activity activity, GridItemModel gridItemModel){
        init(activity, activity.getLayoutInflater().inflate(R.layout.grid_item, null), gridItemModel);
    }

    public GridItemController(Activity activity, View view, GridItemModel gridItemModel){
        if(view == null)
            view = activity.getLayoutInflater().inflate(R.layout.grid_item, null);
        init(activity, view, gridItemModel);
    }

    private void init(Activity activity, View view, GridItemModel gridItemModel){
        this.view = view;
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        TextView id = (TextView) view.findViewById(R.id.id);
        Loader.with(activity).load(gridItemModel.user.profile_image.large).into(imageView);
        id.setText(gridItemModel.id);
    }

    public View getView() {
        return view;
    }
}
