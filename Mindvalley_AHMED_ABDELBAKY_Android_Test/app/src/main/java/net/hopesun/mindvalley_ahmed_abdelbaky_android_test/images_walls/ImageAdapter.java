package net.hopesun.mindvalley_ahmed_abdelbaky_android_test.images_walls;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public class ImageAdapter extends BaseAdapter {
    private final List<GridItemModel> gridItems;
    private Activity activity;

    public ImageAdapter(Activity activity, List<GridItemModel> gridItems) {
        this.activity = activity;
        this.gridItems = gridItems;
    }

    public int getCount() {
        return gridItems.size();
    }

    public GridItemModel getItem(int position) {
        return gridItems.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return new GridItemController(activity, convertView, getItem(position)).getView();
    }
}