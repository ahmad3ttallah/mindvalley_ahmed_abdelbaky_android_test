package net.hopesun.mindvalley_ahmed_abdelbaky_android_test;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

import com.abdelbaky.resourcesloader.lib.*;

import net.hopesun.mindvalley_ahmed_abdelbaky_android_test.images_walls.GridItemModel;
import net.hopesun.mindvalley_ahmed_abdelbaky_android_test.images_walls.ImageAdapter;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String dataResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "This is a fab button", Snackbar.LENGTH_LONG).setAction("Fab Action", null).show();
            }
        });

        dataResource = "http://pastebin.com/raw/wgkJgazE";

        /*
        * Initialize cash limits
        */
        Loader.with(this).setSizeInBytesLimit(5000).setLifeTime(60 * 1000);

        final GridView gridView = (GridView)findViewById(R.id.gridView);
        final SwipeRefreshLayout swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /*
                * Because refresh means get latest from backend so we need to remove cash for relevant source
                */
                Loader.with(MainActivity.this).removeCashedResource(dataResource);

                loadData(gridView, new DoneCallback() {
                    @Override
                    public void done() {
                        swipeToRefresh.setRefreshing(false);
                    }
                });
            }
        });

        loadData(gridView);
    }

    /*
    * This function is to initialize grid view with data for the first time
    * */
    private void loadData(final GridView gridView){
        loadData(gridView, null);
    }

    /*
    * This function is to initiate grid view with details and callback function that can be used on refresh actions
    * */
    private void loadData(final GridView gridView, final DoneCallback doneCallBack){
        Loader.with(this).load(dataResource).into(new Loader.JSONCallback() {
            @Override
            public void json(String json) {
                if(json != null){
                    try {
                        List<GridItemModel> gridItemModels = GridItemModel.parseList(json);
                        // ---------- This is just to test scrolling and check if same resources are from cashed not a new backend call -----
                        gridItemModels.addAll(gridItemModels);
                        gridItemModels.addAll(gridItemModels);
                        // ---------- This is just to test scrolling and check if same resources are from cashed not a new backend call -----
                        gridView.setAdapter(new ImageAdapter(MainActivity.this, gridItemModels));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(doneCallBack != null)
                        doneCallBack.done();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
