package net.hopesun.mindvalley_ahmed_abdelbaky_android_test;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.abdelbaky.resourcesloader.lib.Loader;

public class ActivityOther extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
//        Loader loader = Loader.with(this);
//        loader.setLifeTime(123456789);
//        loader.setSizeInBytesLimit(1234);

        Loader.with(this).load("https://www.gravatar.com/avatar/325049fafa05bc25689058c86428f835?s=32&d=identicon&r=PG").into((ImageView)findViewById(R.id.imageView));
        Loader.with(this).load("http://pastebin.com/raw/wgkJgazE").into(new Loader.JSONCallback() {
            @Override
            public void json(String json) {
                Toast.makeText(ActivityOther.this, json, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
